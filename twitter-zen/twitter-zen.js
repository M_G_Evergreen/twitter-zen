function get_my_name() {
	let account_switcher = document.querySelectorAll('[data-testid="SideNav_AccountSwitcher_Button"]')[0];
	if (!account_switcher)
	{
		return "Anon";
	}
	return account_switcher.children[1].children[0].children[1].innerText;
}

setTimeout(function() {

	let tuneout_message = document.createElement("h1");
	tuneout_message.innerText = "Today is a good day, " + get_my_name() + ".";

	tuneout_message.style = `
		font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Ubuntu, "Helvetica Neue", sans-serif;
		font-size: 19px;
		text-align: center;
		padding-top: 20px;
		font-weight: bold;
		-webkit-animation-name: fade;
		animation-name: fade;
  		-webkit-animation-fill-mode:forwards;
  		animation-fill-mode:forwards;
		animation-duration: 1s;
	`;

	if (document.body.style.backgroundColor == "rgb(255, 255, 255)")
	{
		tuneout_message.style.color += "#000";
	}
	else
	{
		tuneout_message.style.color = "#ffffff";
	}

	let trending_explore = document.querySelectorAll('[aria-label="Timeline: Explore"]')[0];

	if (trending_explore)
	{
		trending_explore.parentNode.replaceChild(tuneout_message, trending_explore);
	}
}, 2500);
