# Twitter Zen

![Logo](promo_large.png)
Twitter's "Explore" sidebar is angry and restless. This extension replaces the Explore/Trending tabs for a more peaceful viewing experience.

# Examples

![Before](screen_before.png)
_Extension off_

![After](screen_after.png)
_Extension on_

!["Good day" message](screen_good_day.png)
_Explore tab now provides a positive message to get your day started right_

# Download
[Download it for **Firefox**](https://addons.mozilla.org/en-US/firefox/addon/twitter-zen/)

[Download it for **Chrome**](https://chrome.google.com/webstore/detail/opkhleeageokmaebaggchkpmlpillocl)

Have I missed a browser? Open an issue, it will be handled.

# Guarantees

I will not steal your data or make your computer behave in unexpected ways.

Feel free to look at the source code, it is simple and understandable.
